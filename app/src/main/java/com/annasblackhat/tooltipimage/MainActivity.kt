package com.annasblackhat.tooltipimage

import android.content.Context
import android.graphics.Point
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.PopupWindow
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.kcrimi.tooltipdialog.ToolTipDialog
import com.skydoves.balloon.*
import it.sephiroth.android.library.xtooltip.ClosePolicy
import it.sephiroth.android.library.xtooltip.Tooltip
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_tooltip.view.*
import java.text.NumberFormat

class MainActivity : AppCompatActivity() {

    private val tooltipList = ArrayList<Tooltip>()
    private var isTooltipShowing = false
    private var newX: Float = 0.toFloat()
    private var newY = 0.toFloat()
    private var popupWindow: PopupWindow? = null
    private val popupWindowList = ArrayList<PopupWindow>()

    /*note
    try create any view positioned at x and y
    https://stackoverflow.com/questions/39257545/make-x-y-work-the-same-on-all-devices-android-studio
    https://stackoverflow.com/questions/1016896/how-to-get-screen-dimensions-as-pixels-in-android
    --*/

    private val toolTipListener = object : ToolTipDialog.ToolTipListener {
        override fun onClickToolTip() {
            Toast.makeText(this@MainActivity, "You've clicked tooltips", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        img.setImageResource(R.drawable.base_image)

        //https://i.postimg.cc/15fSS3td/base-image.jpg
        Glide.with(this)
            .load("https://i.postimg.cc/15fSS3td/base-image.jpg")
            .into(img)

        btn_refresh.setOnClickListener { printBasicInfo() }
        img.setOnClickListener {
//            if (tooltipList.isEmpty())
//                createToolTipList()
//            if (isTooltipShowing) {
//                dismisToolTip()
//            } else {
//                showToolTip()
//            }

            val location = intArrayOf(0, 0)
            it.getLocationInWindow(location)

            val ok = ToolTipDialog(this, this)
                .title("This is a basic dialog") // Define the title for the tooltip
                .content("This dialog will show at the top of the screen by default") // Body content
                .subtitle("Subtitle for tooltip") // Subtitle on the tooltip
                .pointTo(100 + img.width, 100 + img.height)

            ok.setContentView(R.layout.layout_tooltip)
//            ok.show()

//            Toast.makeText(this, "img clicked...", Toast.LENGTH_SHORT).show()

//            showBalloon()
            showCustomView()
            isTooltipShowing = !isTooltipShowing
        }

        img.setOnTouchListener { v, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_DOWN) {
                val location = intArrayOf(0, 0)
                img.getLocationInWindow(location)

                edt_x.setText("${motionEvent.x}")
                edt_y.setText("${motionEvent.y + location[1]}")
                newX = motionEvent.x
                newY = motionEvent.y + location[1]
            } else if (motionEvent.action == MotionEvent.ACTION_UP) {
//                v.performClick()
            }

            false
        }

        btn.setOnClickListener {
            val tooltip = Tooltip.Builder(this)
                .anchor(newX.toInt(), newY.toInt())
                .arrow(true)
                .text("Item ${tooltipList.size}")
                .customView(R.layout.layout_tooltip, R.id.txt_title)
                .overlay(false)
                .closePolicy(ClosePolicy.TOUCH_ANYWHERE_NO_CONSUME)
                .create()
            tooltipList.add(tooltip)
            showToolTip()
        }
//        btn.isEnabled = false

    }

    private fun printBasicInfo() {
        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        var info = "screen width: ${size.x}, height: ${size.y}\n"

        val location = intArrayOf(0, 0)
        img.getLocationInWindow(location)
        info += "img position: ${location[0]} (${img.x}), ${location[1]} (${img.y})\n"

        info += "img width: ${img.width} (${img.measuredWidth}) | height: ${img.height} (${img.measuredHeight})"

        txt_info.text = info
    }

    private fun showCustomView() {
        val location = intArrayOf(0, 0)
        img.getLocationInWindow(location)
        val defaultWidth = 175

        createItem().takeIf { !isTooltipShowing }?.forEach { item ->
            val v = LayoutInflater.from(this).inflate(R.layout.layout_tooltip, null)
            v.findViewById<TextView>(R.id.txt_product_name).text = item.productName
            v.findViewById<TextView>(R.id.txt_price).text =
                "Rp${NumberFormat.getInstance().format(item.price)}"
            v.setOnClickListener {
                Toast.makeText(
                    this,
                    "clicked: ${item.productName}",
                    Toast.LENGTH_SHORT
                ).show()
            }

            val x = (location[0] + (item.xPercentage * img.width)).toInt()
            val y = (location[1] + (item.yPercentage * img.height)).toInt()

            popupWindow = PopupWindow(this)
            popupWindow?.setBackgroundDrawable(null)
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                popupWindow?.setWindowLayoutMode(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
            }

//            popupWindow?.isOutsideTouchable = true
            popupWindow?.contentView = v
            popupWindow?.animationStyle = R.style.popup_window_animation

            popupWindow?.showAtLocation(
                img,
                Gravity.NO_GRAVITY,
                x - (getWidth(v)/2), //x - (defaultWidth/2)
                y
            )

//            v.viewTreeObserver.addOnGlobalLayoutListener(
//                ToolTipTreeObserver(
//                    popupWindow!!,
//                    v,
//                    x,
//                    y
//                )
//            )
            popupWindowList.add(popupWindow!!)
//            v.viewTreeObserver.addOnGlobalLayoutListener(object :
//                ViewTreeObserver.OnGlobalLayoutListener {
//                override fun onGlobalLayout() {
//                    v.viewTreeObserver.removeOnGlobalLayoutListener(this)
//
//                    //resources.getInteger(android.R.integer.config_shortAnimTime)//
//                    popupWindow?.animationStyle = R.style.popup_window_animation
//                    popupWindow?.update(
//                        x - (v.measuredWidth / 2),
//                        y,
//                        v.measuredWidth,
//                        v.measuredHeight
//                    )
//                }
//            })
        } ?: kotlin.run {
            popupWindowList.forEach { it.dismiss() }
            popupWindowList.clear()
        }
//
//
//        val x = (location[0] + (0.39 * img.width)).toInt()
//        val y = (location[1] + (0.46 * img.height)).toInt()
//
//        popupWindow?.let {
//            it.dismiss()
//            popupWindow = null
//            Toast.makeText(this, "dismis...", Toast.LENGTH_SHORT).show()
//        } ?: run {
//            popupWindow = PopupWindow(this)
//            popupWindow?.setBackgroundDrawable(null)
//            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
//                popupWindow?.setWindowLayoutMode(
//                    ViewGroup.LayoutParams.WRAP_CONTENT,
//                    ViewGroup.LayoutParams.WRAP_CONTENT
//                )
//            }
//
//            popupWindow?.contentView = v
//            popupWindow?.showAtLocation(
//                img,
//                Gravity.NO_GRAVITY,
//                x,
//                y
//            )
//
//            v.viewTreeObserver.addOnGlobalLayoutListener(object :
//                ViewTreeObserver.OnGlobalLayoutListener {
//                override fun onGlobalLayout() {
//                    v.viewTreeObserver.removeOnGlobalLayoutListener(this)
//
//                    //resources.getInteger(android.R.integer.config_shortAnimTime)//
//                    popupWindow?.animationStyle = R.style.popup_window_animation
//                    popupWindow?.update(
//                        x - (v.measuredWidth / 2),
//                        y,
//                        v.measuredWidth,
//                        v.measuredHeight
//                    )
//                }
//            })
//
//            Toast.makeText(this, "show... ${v.width}", Toast.LENGTH_SHORT).show()
//        }
    }

    class ToolTipTreeObserver(val popupWindow: PopupWindow, val vi: View, val x: Int, val y: Int) :
        ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            vi.viewTreeObserver.removeOnGlobalLayoutListener(this)
            popupWindow.animationStyle = R.style.popup_window_animation
            popupWindow.update(
                x - (vi.measuredWidth / 2),
                y,
                vi.measuredWidth,
                vi.measuredHeight
            )
            println("measuredWidth: ${vi.measuredWidth}")
        }

    }

    private fun getWidth(view: View): Int {
        val wm = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay

        val size = Point()
        display.getSize(size)
        val deviceWidth = size.x

        val widthMeasureSpec =
            View.MeasureSpec.makeMeasureSpec(deviceWidth, View.MeasureSpec.AT_MOST)
        val heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        view.measure(widthMeasureSpec, heightMeasureSpec)
        return view.measuredWidth
    }

    private fun showBalloon() {
//        val balloon = createBalloon(this) {
//            setArrowSize(10)
//            setWidth(BalloonSizeSpec.WRAP)
//            setHeight(65)
//            setArrowPosition(0.7f)
//            setCornerRadius(4f)
//            setAlpha(0.9f)
//            setText("You can access your profile from now on.")
//            setTextIsHtml(true)
////            setIconDrawable(ContextCompat.getDrawable(context, R.drawable.ic_profile))
//            setBackgroundColorResource(R.color.colorPrimary)
////            setOnBalloonClickListener(onBalloonClickListener)
//            setBalloonAnimation(BalloonAnimation.FADE)
//            setLifecycleOwner(lifecycleOwner)
//        }

        val displayMetrics = DisplayMetrics()
        val wm = applicationContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        wm.defaultDisplay.getMetrics(displayMetrics)
        val maxWidth = displayMetrics.widthPixels
        val maxHeight = displayMetrics.heightPixels
        val moveX = 0.30
        val moveY = 0.30

        val positionY = img.y + maxWidth * moveY
        val positionX = 100


        val balloon = Balloon.Builder(this)
            .setArrowSize(10)
            .setArrowOrientation(ArrowOrientation.TOP)
            .setArrowConstraints(ArrowConstraints.ALIGN_ANCHOR)
            .setArrowPosition(0.5f)
            .setWidth(BalloonSizeSpec.WRAP)
            .setHeight(65)
            .setTextSize(15f)
            .setCornerRadius(4f)
            .setAlpha(0.7f)
            .setWidth(150)
            .setHeight(70)
            .setText("this with X $positionX and Y ${positionY}.")
            .setTextColor(ContextCompat.getColor(this, R.color.colorAccent))
            .setTextIsHtml(true)
//            .setIconDrawable(ContextCompat.getDrawable(context, R.drawable.ic_profile))
            .setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            .setOnBalloonClickListener(OnBalloonClickListener {
                Toast.makeText(this, "balloon clicked...", Toast.LENGTH_SHORT).show()
            })
            .setBalloonAnimation(BalloonAnimation.FADE)
            .setLifecycleOwner(this)
            .build()
        balloon.show(content_view, 0, 0)


        Toast.makeText(this, "$positionX vs $positionY", Toast.LENGTH_SHORT).show()

//        val balloon2 = Balloon.Builder(this)
//            .setArrowSize(10)
//            .setArrowOrientation(ArrowOrientation.TOP)
//            .setArrowConstraints(ArrowConstraints.ALIGN_ANCHOR)
//            .setArrowPosition(0.5f)
//            .setWidth(BalloonSizeSpec.WRAP)
//            .setHeight(65)
//            .setTextSize(15f)
//            .setCornerRadius(4f)
//            .setAlpha(0.9f)
//            .setWidth(200)
//            .setHeight(100)
//            .setText("You can access your profile from now on.")
//            .setTextColor(ContextCompat.getColor(this, R.color.colorAccent))
//            .setTextIsHtml(true)
////            .setIconDrawable(ContextCompat.getDrawable(context, R.drawable.ic_profile))
//            .setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
//            .setOnBalloonClickListener(OnBalloonClickListener {
//                Toast.makeText(this, "balloon clicked...", Toast.LENGTH_SHORT).show()
//            })
//            .setBalloonAnimation(BalloonAnimation.FADE)
//            .setLifecycleOwner(this)
//            .build()
//        balloon2.show(img)
    }

    private fun createToolTipList() {
        val location = intArrayOf(0, 0)
        img.getLocationInWindow(location)

        tooltipList.clear()
        listOf(
            Item("Paper Cup", location[0] + img.width / 2, location[1] + img.height),
            Item("Pet Cup", 741, 526),
            Item("Botol PET", 240, 539)
        ).forEach { item ->
            val tooltip = Tooltip.Builder(this)
                .anchor(item.x, item.y)
                .text(item.productName)
                .customView(R.layout.layout_tooltip, R.id.txt_title)
                .overlay(false)
                .closePolicy(ClosePolicy.TOUCH_ANYWHERE_NO_CONSUME)
                .create()
//            tooltip.show()
            tooltipList.add(tooltip)
        }
    }

    private fun dismisToolTip() {
        isTooltipShowing = false
        tooltipList.forEach { tooltip -> tooltip.dismiss() }
    }

    private fun showToolTip() {
        isTooltipShowing = true
//        ToolTipDialog(this, this)
//            .title("Pet Cup")
//            .content("Rp13,000")
//            .pointTo(729, 355)
//            .show()

//        val vi = View(this)
//        vi.x = 729.0f
//        vi.y = 355.0f
//        PopupMenu(this)
//
        tooltipList.forEach { tooltip ->
            tooltip.show(img, Tooltip.Gravity.BOTTOM, false)
            println("tooltip contentView: ${tooltip.contentView}")
            val vi = tooltip.contentView!!
            vi.txt_price.text = "Rp${tooltip.offsetX}${tooltip.offsetY}"
//            vi.txt_title.setOnClickListener {
//                println("text click...")
//            }
            tooltip.contentView?.setOnClickListener {
                println("tooltip click...")
            }
        }
    }

    private fun hideKeyboard() {
        currentFocus?.apply {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imm?.hideSoftInputFromWindow(windowToken, 0)
        }
    }

    override fun onResume() {
        super.onResume()
        printBasicInfo()
//        hideKeyboard()
//        currentFocus?.clearFocus()
    }

    private fun createItem(): List<Item> {
        return listOf(
            Item("Botol PET", xPercentage = 0.15, yPercentage = 0.4266, price = 15000),
            Item("Hot Paper Cup", price = 2500, xPercentage = 0.37, yPercentage = 0.79),
            Item("PET Cup", price = 2350, xPercentage = 0.73, yPercentage = 0.43),
            Item("PP Cup", price = 1300, xPercentage = 0.4266, yPercentage = 0.5166)
        )
    }

    data class Item(
        var productName: String,
        var x: Int = 0,
        var y: Int = 0,
        var price: Int = 0,
        var xPercentage: Double = 0.0,
        var yPercentage: Double = 0.0
    )
}
